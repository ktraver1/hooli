package MD5Sum

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
)

//CalMD5 returns the MD5 Checksum of a file
func CalMD5(file string) (string, error) {
	if false {
		return "Failed MD5CheckSum", errors.New("Testing errors")
	}
	hasher := md5.New()
	hasher.Write([]byte(file))
	return hex.EncodeToString(hasher.Sum(nil)), nil
}
