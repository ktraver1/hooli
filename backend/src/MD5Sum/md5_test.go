package MD5Sum

import (
	"fmt"
	"testing"
)

func TestMD5(t *testing.T) {
	var md5sum, _ = CalMD5("Test")
	fmt.Println(md5sum)
	if md5sum == "Failed MD5CheckSum" {
		t.Error("Expected False, got ", md5sum)
	}
}
