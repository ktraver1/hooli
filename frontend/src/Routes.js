import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./containers/Home/Home";
import Template from "./containers/Template/Template";
import NotFound from "./containers/NotFound/NotFound";

export default () =>
  <Switch>
    { /* need 2 paths home for navbar to work*/ }
    <Route path="/" exact component={Home} />
    <Route path="/home" exact component={Home} />
    <Route path="/template/" exact component={Template} />
    { /* Finally, catch all unmatched routes */ }
    <Route component={NotFound} />
  </Switch>;
