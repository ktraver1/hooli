import React from 'react';
import './App.css';
import Routes from "./Routes";

function App() {
  return (
    <div className="App">
        <Routes />
        <h>Welcome To Hooli</h>
    </div>
  );
}

export default App;
